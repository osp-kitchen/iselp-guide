# Guide d’usage 

## Logo
Petit mot sur le logo

- Le dessin du logo existe en 4 versions différentes. Ces variantes sont toutes à activer. L'une des versions a été retenue pour le pin's mais n'est pas pour autant prioritaire.
- En dehors du pin's le logo reste noir et blanc.
- Lorsqu'il apparaît sur une photo ou un fond de couleur ou image, les lettres ont un fond blanc.


## Échelles

- Sur support imprimé le logo existe actuellement à  2 échelles : 
    - taille du pin's 
    - taille 2 à préciser ici
- De la même manière, 2 tailles de corps différentes sont utilisées pour la mise en forme du texte.
- Pour le print voir → cf templates flyers et posters


